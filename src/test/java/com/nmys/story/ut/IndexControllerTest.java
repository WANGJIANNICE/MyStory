package com.nmys.story.ut;

import com.github.pagehelper.PageInfo;
import com.nmys.story.controller.IndexController;
import com.nmys.story.model.entity.Comments;
import com.nmys.story.model.entity.Contents;
import com.nmys.story.service.ICommentService;
import com.nmys.story.service.IContentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author Ryan Zhang
 * @Title: IndexControllerTest
 * @Description: 前台首页单元测试
 * @date 2019/6/3
 * @From https://www.zhangjianbing.com
 */
@RunWith(MockitoJUnitRunner.class)
public class IndexControllerTest {

    @InjectMocks
    private IndexController indexController;

    @Mock
    private IContentService contentService;

    @Mock
    private ICommentService commentService;


    /**
     * 自定义页面,如：前台关于页面<单元测试>
     */
    @Test
    public void pageTest() {
        Contents contents1 = new Contents();
        contents1.setAllowComment(1);
        contents1.setCid(1);
        contents1.setHits(2);
        when(contentService.getContentBySlug(any(String.class))).thenReturn(contents1);

        when(contentService.updateContent(any(Contents.class))).thenReturn(true);

        when(commentService.getCommentsListByContentId(any(Integer.class),any(Integer.class),any(Integer.class))).thenReturn(new PageInfo<Comments>());

        HttpServletRequest request = new MockHttpServletRequest();
        indexController.page("article", "11", request);
    }

}
